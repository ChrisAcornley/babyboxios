//
//  SongController.swift
//  BabyBoxiOS
//
//  Created by Christopher Acornley on 27/06/2017.
//  Copyright © 2017 Christopher Acornley. All rights reserved.
//

import UIKit
import AVFoundation

class SongController: UIViewController, UITableViewDelegate, UITableViewDataSource, AVAudioPlayerDelegate {
    
    
    //MARK: Linked UI Elements
    @IBOutlet private weak var tracklistTableView: UITableView!
    @IBOutlet private weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var durationLabel: UILabel!
    @IBOutlet private weak var trackTimeLabel: UILabel!
    @IBOutlet private weak var songSlider: UISlider!
    @IBOutlet private weak var playButton: UIButton!
    @IBOutlet private weak var pauseButton: UIButton!
    @IBOutlet private weak var shuffleButton: UIButton!
    @IBOutlet private weak var repeatButton: UIButton!
    @IBOutlet private weak var backButton: UIButton!
    @IBOutlet private weak var skipBackButton: UIButton!
    @IBOutlet private weak var stopButton: UIButton!
    @IBOutlet private weak var skipForwardButton: UIButton!
    @IBOutlet private weak var buttonStackView: UIStackView!
    
    //MARK: Private Properties
    private var musicPlayer : AVAudioPlayer?
    private var updater : CADisplayLink! = nil
    private var shuffle : Bool = false
    private var repeatSong : Bool = false
    private var pressedRepeatImage : UIImage?
    private var pressedShuffleImage : UIImage?
    private var defaultRepeatImage : UIImage?
    private var defaultShuffleImage : UIImage?
    private var currentSong : Song?
    private var songIndex : Int = 0
    private var shufflePlaylist = [Song]()
    private var currentCell : TrackSelectionTableCell?
    private var labelFont : UIFont?
    
    //MARK: Public Properties
    public var playlist = [Song]()
    public var backgroundType : String = ""
    public var scaleUI : CGFloat = 1.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set background image based on playlist chosen
        backgroundImageView.image = UIImage(named: self.backgroundType+"Background")
        
        // Store default colours
        pressedRepeatImage = UIImage(named: "DarkRepeatIcon")
        pressedShuffleImage = UIImage(named: "DarkShuffleIcon")
        defaultRepeatImage = repeatButton.currentImage
        defaultShuffleImage = shuffleButton.currentImage
        
        // Create updater for UISlider and timers
        updater = CADisplayLink(target: self, selector: #selector(trackAudio))
        updater.frameInterval = 1
        updater.add(to: .current, forMode: .defaultRunLoopMode)
        updater.isPaused = true
        
        // Link this ViewController to be notified when the app enters background
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)

        // Load and play first song
        loadSong(playlist[0])
        updateViews()
        pauseSong()
        
        resetTableSelectionBox()
        
        let newFont = UIFont(name: trackTimeLabel.font.fontName, size: CGFloat(Int(trackTimeLabel.font.pointSize * scaleUI)))
        trackTimeLabel.font = newFont
        durationLabel.font = newFont
        songSlider.contentScaleFactor = scaleUI
        
        tracklistTableView.rowHeight = tracklistTableView.rowHeight*scaleUI
        
        buttonStackView.spacing = buttonStackView.spacing*scaleUI
        
        let fontSize : Int = Int(17.0*self.scaleUI)
        labelFont = UIFont(name: ".SFUIText", size: CGFloat(fontSize))
        
        backButton.contentVerticalAlignment = .fill
        backButton.contentHorizontalAlignment = .fill
        shuffleButton.contentVerticalAlignment = .fill
        shuffleButton.contentHorizontalAlignment = .fill
        skipBackButton.contentVerticalAlignment = .fill
        skipBackButton.contentHorizontalAlignment = .fill
        stopButton.contentVerticalAlignment = .fill
        stopButton.contentHorizontalAlignment = .fill
        playButton.contentVerticalAlignment = .fill
        playButton.contentHorizontalAlignment = .fill
        pauseButton.contentVerticalAlignment = .fill
        pauseButton.contentHorizontalAlignment = .fill
        skipForwardButton.contentVerticalAlignment = .fill
        skipForwardButton.contentHorizontalAlignment = .fill
        repeatButton.contentVerticalAlignment = .fill
        repeatButton.contentHorizontalAlignment = .fill
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        // Check to make sure we get the signal for a popup
        switch(segue.identifier ?? "") {
        case "ShowLyrics":
            // Display lysics
            guard let lyricsPopup = segue.destination as? LyricPopup else {
                fatalError("Unexpected detination: \(segue.destination)")
            }
            
            guard let selectedCell = sender as? TrackSelectionTableCell else {
                fatalError("Unexpected sender: \(sender)")
            }
            
            lyricsPopup.song = selectedCell.song!
            lyricsPopup.scaleUI = self.scaleUI
            songIndex = selectedCell.playButton.tag
            
            if currentSong?.name != selectedCell.song!.name {
                loadSong(selectedCell.song!)
                updateViews()
                playSong()
            }
            
            break
        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
        }
    }
    
    @IBAction func onBackButtonPressed(_ sender: Any) {
        // Clear current song and pop current view from stack
        pauseSong()
        self.navigationController!.popViewController(animated: true)
    }
    
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {
        return playlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let currentSong : Song = playlist[indexPath.row]
        // Cell ID
        let cellIdentifier = "TrackSelectionTableCell"
        
        // Create cell as PlaylistTableViewCell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TrackSelectionTableCell else {
            fatalError("The dequeued cell is not an instance of TrackSelectionTableCell.")
        }
        
        if backgroundType == "Nap" {
            cell.songLabel.textColor = UIColor.white
        }
        
        // Set cell type and image
        cell.songLabel.text = currentSong.name
        cell.songLabel.font = labelFont
        cell.song = currentSong
        cell.playButton.tag = indexPath.row
        cell.pauseButton.tag = indexPath.row

        cell.playButton.contentVerticalAlignment = .fill
        cell.playButton.contentHorizontalAlignment = .fill
        cell.pauseButton.contentVerticalAlignment = .fill
        cell.pauseButton.contentHorizontalAlignment = .fill
        
        resetTableSelectionBox()
        
        // Return cell for use in table view
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        // Deselect row as soon as row is selected
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        resetTableSelectionBox()
    }
    
    private func resetTableSelectionBox() {
        let tableCells = tracklistTableView.visibleCells
        for cell in tableCells {
            let trackCell = cell as? TrackSelectionTableCell
            if trackCell?.song?.name == currentSong?.name {
                trackCell?.playButton.isHidden = (musicPlayer?.isPlaying)!
                trackCell?.playButton.isEnabled = !(musicPlayer?.isPlaying)!
                trackCell?.pauseButton.isHidden = !(musicPlayer?.isPlaying)!
                trackCell?.pauseButton.isEnabled = (musicPlayer?.isPlaying)!
                trackCell?.selectedBox.isHidden = false
            } else {
                trackCell?.playButton.isHidden = false
                trackCell?.playButton.isEnabled = true
                trackCell?.pauseButton.isHidden = true
                trackCell?.pauseButton.isEnabled = false
                trackCell?.selectedBox.isHidden = true
            }
        }
    }
    
    func appMovedToBackground() {
        // Pause sone when app goes into the background
        pauseSong()
    }
    
    func trackAudio() {
        // Update UISlider with time and label
        songSlider.value = Float((musicPlayer?.currentTime)!)
        durationLabel.text = trackTimeString(songSlider.value)
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        // Stop updating the UISlider and timers
        self.updater.isPaused = true
        
        if repeatSong {
            musicPlayer?.currentTime = 0
            playSong()
        }
        else {
            nextSongItem(1)
            let currentController = self.presentedViewController as? LyricPopup
            if currentController != nil {
                currentController?.resetLabels(newSong: currentSong!, trackDuration: trackTimeString(Float((musicPlayer?.duration)!)))
            }
            resetTableSelectionBox()
            playSong()
        }
    }
    
    //MARK: Private Functions
    private func trackTimeString(_ time : Int) ->String {
        // Convert argument to minutes:seconds format
        let minutes = Int(Float(time)/60.0)
        let seconds = time - minutes*60
        
        return (NSString(format : "%02d:%02d", minutes, seconds) as String)
    }
    
    private func trackTimeString(_ time : Float) ->String {
        // Convert argument to minutes:seconds format
        let minutes = Int(time/60)
        let seconds = Int(time) - minutes*60
        
        return (NSString(format : "%02d:%02d", minutes, seconds) as String)
    }
    
    private func playSong() {
        // Call play on the AVMusicPlayer
        musicPlayer?.play()
        
        // Unpause the update function
        updater.isPaused = false
        
        // Hide play and unhide pause button
        playButton.isHidden = true
        playButton.isEnabled = false
        
        pauseButton.isHidden = false
        pauseButton.isEnabled = true
        
        resetTableSelectionBox()
    }
    
    private func pauseSong() {
        // Call pause on the AVMusicPlayer
        musicPlayer?.pause()
        
        // Pause the update function
        updater.isPaused = true
        
        // Hide pause and unhide play button
        playButton.isHidden = false
        playButton.isEnabled = true
        
        pauseButton.isHidden = true
        pauseButton.isEnabled = false
        
        resetTableSelectionBox()
    }
    
    private func loadSong(_ song : Song) {
        if let musicItem = NSDataAsset(name: song.urlName) {
            do {
                musicPlayer = try AVAudioPlayer(data: musicItem.data, fileTypeHint: AVFileTypeMPEGLayer3)
                guard let musicPlayer = musicPlayer else {
                    fatalError("OMG something is not right")
                }
                currentSong = song
                musicPlayer.prepareToPlay()
                musicPlayer.delegate = self
            } catch let error {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    private func nextSongItem(_ increment : Int) {
        songIndex += increment
        
        let currentPlaylist = shuffle ? shufflePlaylist : playlist
        
        if songIndex < 0 {
            songIndex = (currentPlaylist.count - 1)
        }
        else if songIndex >= currentPlaylist.count {
            songIndex = 0
        }
        
        loadSong(currentPlaylist[songIndex])
        updateViews()
    }
    
    private func createShuffleList() {
        var newShufflePlaylist = playlist
        if let index = newShufflePlaylist.index(where: { (item) -> Bool in
            item.name == currentSong?.name }) {
            newShufflePlaylist.remove(at: index)
            
            shufflePlaylist = newShufflePlaylist.shuffled()
            shufflePlaylist.insert(currentSong!, at: 0)
            
            songIndex = 0
        }
        else {
            print("Huoston, we have a problem!")
        }
    }
    
    private func updateViews() {
        // From current track - load time and set up slider
        durationLabel.text = trackTimeString(0.0)
        trackTimeLabel.text = trackTimeString(Float((musicPlayer?.duration)!))
        songSlider.maximumValue = Float((musicPlayer?.duration)!)
        songSlider.value = 0.0
        resetTableCell()
    }
    
    private func resetTableCell() {
        let tableCells = tracklistTableView.visibleCells
        for cell in tableCells {
            let trackCell = cell as? TrackSelectionTableCell
            trackCell?.resetButtons()
            if trackCell?.song?.name == currentSong?.name {
                currentCell = trackCell
            }
        }
    }
    
    private func setStatusOfCellButtons(isPlaying : Bool) {
        currentCell?.playButton.isHidden = isPlaying
        currentCell?.playButton.isEnabled = !isPlaying
        currentCell?.pauseButton.isHidden = !isPlaying
        currentCell?.pauseButton.isEnabled = isPlaying
        currentCell?.selectedBox.isHidden = false
    }
    
    //MARK: UI Functions
    @IBAction func onShuffleButtonPressed(_ sender: Any) {
        // If we are set to shuffle through songs, on press set this to false and reset button
        // If we are not set to shuffle, set it to true and make visible change to button
        if shuffle {
            shuffleButton.setImage(defaultShuffleImage, for: .normal)
            self.shuffle = false
        }
        else {
            shuffleButton.setImage(pressedShuffleImage, for: .normal)
            repeatButton.setImage(defaultRepeatImage, for: .normal)
            self.shuffle = true
            self.repeatSong = false
            createShuffleList()
        }
    }
    
    @IBAction func onRepeatButtonPressed(_ sender: Any) {
        // If we are set to repeat through songs, on press set this to false and reset button
        // If we are not set to repeat, set it to true and make visible change to button
        if repeatSong {
            repeatButton.setImage(defaultRepeatImage, for: .normal)
            self.repeatSong = false
        }
        else {
            repeatButton.setImage(pressedRepeatImage, for: .normal)
            shuffleButton.setImage(defaultShuffleImage, for: .normal)
            self.shuffle = false
            self.repeatSong = true
        }
    }
    
    @IBAction func onPreviousTrackPressed(_ sender: Any) {
        // If we are at least one second into the current song, start from the beginning
        // Else go to the previous song
        if (musicPlayer?.currentTime)! > Double(2) {
            musicPlayer?.currentTime = 0
        } else {
            nextSongItem(-1)
            playSong()
        }
    }
    
    @IBAction func onNextTrackPressed(_ sender: Any) {
        // Jump to next song
        nextSongItem(1)
        playSong()
    }
    
    @IBAction func onStopButtonPressed(_ sender: Any) {
        // Stop the current song
        pauseSong()
        musicPlayer?.currentTime = 0
        trackAudio()
    }
    
    @IBAction func onPauseButtonPressed(_ sender: Any) {
        // Pause the current song
        pauseSong()
    }
    
    @IBAction func onPlayButtonPressed(_ sender: Any) {
        // Start playing the song
        playSong()
    }
    
    @IBAction func onSliderTouchStarted(_ sender: UISlider) {
        // When the UISlider is touched, we stop if from being updated
        updater.isPaused = true
    }
        
    @IBAction func onSliderValueUpdated(_ sender: UISlider) {
        // Update the song timer text as the user makes changes
        durationLabel.text = trackTimeString(songSlider.value)
    }
        
    @IBAction func onSliderTouchFinished(_ sender: UISlider) {
        // Set current time to slider value, update timer text and unpause the update function
        musicPlayer?.currentTime = Double(songSlider.value)
        durationLabel.text = trackTimeString(songSlider.value)
        updater.isPaused = false
    }
    
    @IBAction func onCellPauseButtonPressed(_ sender: Any) {
        let uiButton : UIButton = sender as! UIButton
        let indexPath : IndexPath = IndexPath(row: uiButton.tag, section: 0)
        guard let cell : TrackSelectionTableCell = tracklistTableView.cellForRow(at: indexPath) as? TrackSelectionTableCell else {
            fatalError("Cannot get cell from table")
        }
        
        if cell.song?.name == currentSong?.name {
            pauseSong()
            cell.pauseButton.isHidden = true
            cell.playButton.isHidden = false
            
            cell.pauseButton.isEnabled = false
            cell.playButton.isEnabled = true
        }
    }
    
    @IBAction func onCellPlayButtonPressed(_ sender: Any) {
        let uiButton : UIButton = sender as! UIButton
        let indexPath : IndexPath = IndexPath(row: uiButton.tag, section: 0)
        guard let cell : TrackSelectionTableCell = tracklistTableView.cellForRow(at: indexPath) as? TrackSelectionTableCell else {
            fatalError("Cannot get cell from table")
        }
        
        resetTableCell()
        
        if cell.song?.name == currentSong?.name {
            playSong()
        } else {
            songIndex = uiButton.tag
            loadSong(cell.song!)
            updateViews()
            playSong()
        }
    }
}
