//
////
////  PlaylistCollectionTableView.swift
////  BabyBoxiOS
////
////  Created by Christopher Acornley on 29/05/2017.
////  Copyright © 2017 Christopher Acornley. All rights reserved.
////
//
//import UIKit
//
//class PlaylistCollectionTableView: UITableViewController {
//
//    private var wakePlaylist = [Song]()
//    private var playPlaylist = [Song]()
//    private var napPlaylist = [Song]()
//    private var buttonNames = [String]()
//    private var scaleUI : CGFloat = 1.0
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        buttonNames = ["Wake", "Play", "Nap", "All"]
//        
//        // Load the sample data.
//        loadSongsFromJson(playlistName: "WakePlaylist")
//        loadSongsFromJson(playlistName: "PlayPlaylist")
//        loadSongsFromJson(playlistName: "NapPlaylist")
//        
//        self.navigationItem.hidesBackButton = true
//        scaleUI = self.tableView.frame.width / 375.0
//        self.tableView.rowHeight = self.tableView.rowHeight * scaleUI
//        self.tableView.separatorStyle = .none
//        
//        self.tableView.backgroundColor = UIColor(patternImage: UIImage(named: "PlaylistTableViewBackground")!)
//        
//        self.navigationController?.isNavigationBarHidden = true
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    // MARK: - Table view data source
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 1
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 4
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//        let cellIdentifier = "PlaylistTableViewCell"
//        
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PlaylistTableViewCell else {
//            fatalError("The dequeued cell is not an instance of PlaylistTableViewCell.")
//        }
//        
//        cell.cellType = buttonNames[indexPath.row]
//        cell.playlistBackroundImage.image = UIImage(named: (buttonNames[indexPath.row] + "Image"))
//        
//        return cell
//    }
//    
//    // Override to support conditional editing of the table view.
//    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        // Return false if you do not want the specified item to be editable.
//        return false
//    }
//    
//    
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//        
//        super.prepare(for: segue, sender: sender)
//        
//        switch(segue.identifier ?? "") {
//            
////        case "ShowPlaylist":
////            guard let songDetailViewController = segue.destination as? SongTableViewController else {
////                fatalError("Unexpected destination: \(segue.destination)")
////            }
////            
////            songDetailViewController.scaleUI = scaleUI
////            
////            guard let selectedSongCell = sender as? PlaylistTableViewCell else {
////                fatalError("Unexpected sender: \(sender)")
////            }
////            
////            switch(selectedSongCell.cellType) {
////            case "Nap":
////                songDetailViewController.songs = napPlaylist
////            case "Play":
////                songDetailViewController.songs = playPlaylist
////            case "Wake":
////                songDetailViewController.songs = wakePlaylist
////            case "All":
////                songDetailViewController.songs = (napPlaylist + playPlaylist + wakePlaylist)
////            default:
////                fatalError("Unexpected Button Name")
////            }
////            break
////        case "ShowAbout":
////            break
////        default:
////            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
////        }
//    }
//    
//    //MARK: Private Methods
//    private func loadSongsFromJson(playlistName : String) {
//        if let jsonFile = NSDataAsset(name: playlistName) {
//            do {
//                let jsonData = try JSONSerialization.jsonObject(with: jsonFile.data, options: [])
//                if let jsonResult = jsonData as? [String:Any] {
//                    if let array = jsonResult["playlist"] as? [Any] {
//                        for song in array {
//                            loadSongToPlaylist(songJson: (song as? [String:Any])!, playlistName: playlistName)
//                        }
//                    }
//                }
//                
//            } catch {
//                print(error.localizedDescription)
//            }
//        }
//    }
//    
//    private func loadSongToPlaylist(songJson : [String:Any], playlistName : String) {
//        let songName = songJson["name"] as? String
//        let songAuthor = songJson["author"] as? String
//        let songURL = songJson["url"] as? String
//        let songPhoto = UIImage(named: (songJson["photo"] as? String)!)
//        let songDuration = songJson["duration"] as? Int
//        
//        
//        guard let song = Song(name: songName!, author: songAuthor!, url: songURL!, photo: songPhoto, duration: songDuration!) else {
//            fatalError("Cannot create Song object from JSON data")
//        }
//        
//        switch(playlistName) {
//        case "WakePlaylist":
//            wakePlaylist += [song]
//        case "PlayPlaylist":
//            playPlaylist += [song]
//        case "NapPlaylist":
//            napPlaylist += [song]
//        default:
//            fatalError("Cannot append Song to unknown playlist")
//        }
//    }
//}
