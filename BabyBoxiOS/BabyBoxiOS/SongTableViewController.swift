////
////  SongTableViewController.swift
////  BabyBoxiOS
////
////  Created by Christopher Acornley on 5/10/17.
////  Copyright © 2017 Apple Inc. All rights reserved.
////
//
//import UIKit
//import AVFoundation
//
//class SongTableViewController: UITableViewController {
//    
//    //MARK: Properties
//    public var songs = [Song]()
//    public var scaleUI : CGFloat = 1.0
//
//    //MARK: Initialisation
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        self.tableView.rowHeight = self.tableView.rowHeight * scaleUI
//        self.tableView.separatorStyle = .none
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    //MARK: - Table view data source
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return songs.count
//    }
//
//    // Set the spacing between sections
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 5.0
//    }
//    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//        // Table view cells are reused and should be dequeued using a cell identifier.
//        let cellIdentifier = "SongTableViewCell"
//        
////        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SongTableViewCell  else {
////            fatalError("The dequeued cell is not an instance of SongTableViewCell.")
////        }
//        
//        // Fetches the appropriate song for the data source layout.
//        let song = songs[indexPath.row]
//        
////        cell.setLabelFont(newScale: scaleUI)
////        cell.nameLabel.text = song.name
////        cell.photoImageView.image = song.photo
////        cell.authorLabel.text = song.author
////        cell.durationLabel.text = trackTimeString(song.duration)
////        
////        return cell
//        return nil
//    }
//              
//    // Override to support conditional editing of the table view.
//    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
//    {
//        // Return false if you do not want the specified item to be editable.
//        return false
//    }
//    
//    //MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        
//        super.prepare(for: segue, sender: sender)
//        
//        switch(segue.identifier ?? "") {
//            
//        case "ShowDetail":
//            guard let songDetailViewController = segue.destination as? SongViewController else {
//                fatalError("Unexpected destination: \(segue.destination)")
//            }
//            
////            guard let selectedSongCell = sender as? SongTableViewCell else {
////                fatalError("Unexpected sender: \(sender)")
////            }
//            
////            guard let indexPath = tableView.indexPath(for: selectedSongCell) else {
////                fatalError("The selected cell is not being displayed by the table")
////            }
//            
//            //songDetailViewController.songItem = indexPath.row
//            songDetailViewController.playlist = songs
//            songDetailViewController.scaleUI = scaleUI
//            
//        default:
//            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
//        }
//    }
//    
//    //MARK: Private Methods
//    private func trackTimeString(_ time : Int) ->String {
//        let minutes = Int(Float(time)/Float(60))
//        let seconds = time - minutes*60
//        
//        return (NSString(format : "%i:%02d", minutes, seconds) as String)
//    }
//    
//    //MARK: Navigation
//    @IBAction func backButtonPressed(_ sender: Any) {
//        // Make sure we pop the current view from the stack to return to the SongTableViewController
//        if let owningNavigationController = navigationController{
//            owningNavigationController.popViewController(animated: true)
//        }
//        else {
//            fatalError("The SongTableViewController is not inside a navigation controller.")
//        }
//    }
//}
