//
//  TrackSelectionTableCell.swift
//  BabyBoxiOS
//
//  Created by Christopher Acornley on 27/06/2017.
//  Copyright © 2017 Christopher Acornley. All rights reserved.
//

import UIKit

class TrackSelectionTableCell: UITableViewCell {

    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet public weak var songLabel: UILabel!
    @IBOutlet weak var selectedBox: UIImageView!
    public var song : Song? = nil
    public var darkBackground : Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        resetButtons()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func resetButtons() {
        pauseButton.isHidden = true
        pauseButton.isEnabled = false
        
        playButton.isHidden = false
        playButton.isEnabled = true
        
        selectedBox.isHidden = true
    }
}
