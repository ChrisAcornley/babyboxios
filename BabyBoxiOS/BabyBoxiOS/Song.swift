//
//  Song.swift
//  BabyBoxiOS
//
//  Created by Christopher Acornley on 5/10/17.
//  Copyright © 2017 Apple Inc. All rights reserved.
//

import UIKit

class Song {
    
    //MARK: Properties
    
    var name: String
    var author : String
    var urlName : String
    var lyricsString : String
    
    //MARK: Initialization
    
    init?(name: String, author : String, url : String, lyrics : String) {
        
        // The name must not be empty
        guard (!name.isEmpty && !author.isEmpty && !url.isEmpty && !lyrics.isEmpty) else {
            return nil
        }
        
        // Initialize stored properties.
        self.name = name
        self.author = author
        self.urlName = url
        self.lyricsString = lyrics
    }
}
