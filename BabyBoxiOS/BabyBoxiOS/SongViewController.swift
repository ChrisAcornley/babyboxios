////
////  SongViewController.swift
////  BabyBoxiOS
////
////  Created by Christopher Acornley on 5/10/17.
////  Copyright © 2017 Apple Inc. All rights reserved.
////
//
//import UIKit
//import AVFoundation
//
//class SongViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AVAudioPlayerDelegate {
//    
//    //MARK: Properties
//    //MARK: Private
//    @IBOutlet private weak var nameTextField: UILabel!
//    @IBOutlet private weak var authorTextField: UILabel!
//    @IBOutlet private weak var photoImageView: UIImageView!
//    @IBOutlet private weak var songSlider : UISlider!
//    @IBOutlet private weak var songTimerLabel : UILabel!
//    @IBOutlet private weak var songDurationLabel : UILabel!
//    @IBOutlet private weak var playButton : UIButton!
//    @IBOutlet private weak var pauseButton : UIButton!
//    @IBOutlet private weak var repeatButton : UIButton!
//    @IBOutlet private weak var shuffleButton : UIButton!
//    @IBOutlet private weak var stackView : UIStackView!
//    private var musicPlayer : AVAudioPlayer?
//    private var updater : CADisplayLink! = nil
//    private var shuffle : Bool = false
//    private var repeatSong : Bool = false
//    private var defaultShuffleButtonTint : UIColor?
//    private var defaultRepeatButtonTint : UIColor?
//    
//    //MARK: Public
//    var playlist = [Song]()
//    var shufflePlaylist = [Song]()
//    var song: Song?
//    var songItem : Int = 0
//    var scaleUI : CGFloat = 1.0
//    
//    
//    //MARK: Public Functions
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//		// Store default colours
//        defaultRepeatButtonTint = repeatButton.tintColor
//        defaultShuffleButtonTint = shuffleButton.tintColor
//        
//		// Create updater for UISlider and timers
//        updater = CADisplayLink(target: self, selector: #selector(trackAudio))
//        updater.frameInterval = 1
//        updater.add(to: .current, forMode: .defaultRunLoopMode)
//        updater.isPaused = true
//        
//        resizeFonts(newScale: scaleUI)
//        
//		// Link this ViewController to be notified when the app enters background
//        let notificationCenter = NotificationCenter.default
//        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
//
//		// Create shuffle playlist - REVIEW
//        if (playlist.count > 0) && ((songItem >= 0) && (songItem <= (playlist.count - 1))) {
//            loadViews(playlist[songItem])
//            
//            shufflePlaylist = playlist.shuffled()
//        }
//        
//		// Load current song - Set in previous ViewController's prepare function
//        loadSong(song!)
//        playSong()
//        
//        let bufferSize = (self.view.bounds.width - 576.0) / 2.0
//        
//        if(bufferSize > 0.0) {
//            let leftBuffer : UIView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: bufferSize, height: 576.0))
//            let rightBuffer : UIView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: bufferSize, height: 576.0))
//            
//            //leftBuffer.addConstraint(
//             let c1 = NSLayoutConstraint(item: leftBuffer, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: bufferSize)
//            //)
//            //rightBuffer.addConstraint(
//            let c2 = NSLayoutConstraint(item: rightBuffer, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: bufferSize)
//            //)
//            
//            leftBuffer.addConstraint(c1)
//            rightBuffer.addConstraint(c2)
//            
//            stackView.removeArrangedSubview(photoImageView)
//            stackView.addArrangedSubview(leftBuffer)
//            stackView.addArrangedSubview(photoImageView)
//            stackView.addArrangedSubview(rightBuffer)
//        }
//    }
//    
//    func appMovedToBackground() {
//        pauseSong()
//    }
//    
//    func trackAudio() {
//		// Update UISlider with time and label
//        songSlider.value = Float((musicPlayer?.currentTime)!)
//        songTimerLabel.text = trackTimeString(songSlider.value)
//    }
//    
//    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
//		// Stop updating the UISlider and timers
//        self.updater.isPaused = true
//		
//        if repeatSong {
//            musicPlayer?.currentTime = 0
//            playSong()
//        }
//        else {
//            nextSongItem(1)
//        }
//    }
//    
//    //MARK: Navigation
//    @IBAction func cancel(_ sender: UIBarButtonItem) {
//        // Stop music from playing if it already is
//        if (musicPlayer?.isPlaying)! {
//            musicPlayer?.stop()
//        }
//        
//		// Make sure we pop the current view from the stack to return to the SongTableViewController
//        if let owningNavigationController = navigationController{
//            owningNavigationController.popViewController(animated: true)
//        }
//        else {
//            fatalError("The SongViewController is not inside a navigation controller.")
//        }
//    }
//    
//    //MARK: Linked Functions
//    @IBAction func shuffleButtonTouched(_ sender: UIButton) {
//		// If we are set to shuffle through songs, on press set this to false and reset button
//		// If we are not set to shuffle, set it to true and make visible change to button
//        if shuffle {
//            shuffleButton.tintColor = defaultShuffleButtonTint
//            self.shuffle = false
//        }
//        else {
//            shuffleButton.tintColor = UIColor(colorLiteralRed: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
//            repeatButton.tintColor = defaultRepeatButtonTint
//            self.shuffle = true
//            self.repeatSong = false
//            createShuffleList()
//        }
//    }
//    
//    @IBAction func repeatButtonTouched(_ sender: UIButton) {
//		// If we are set to repeat through songs, on press set this to false and reset button
//		// If we are not set to repeat, set it to true and make visible change to button
//        if repeatSong {
//            repeatButton.tintColor = defaultRepeatButtonTint
//            self.repeatSong = false
//        }
//        else {
//            repeatButton.tintColor = UIColor(colorLiteralRed: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
//            shuffleButton.tintColor = defaultShuffleButtonTint
//            self.shuffle = false
//            self.repeatSong = true
//        }
//    }
//    
//    @IBAction func forwardButtonTouched(_ sender: UIButton) {
//		// Jump to next song
//        nextSongItem(1)
//    }
//    
//    @IBAction func backwardButtonTouched(_ sender: UIButton) {
//		// If we are at least one second into the current song, start from the beginning
//		// Else go to the previous song
//        if (musicPlayer?.currentTime)! > Double(1) {
//            musicPlayer?.currentTime = 0
//        } else {
//            nextSongItem(-1)
//        }
//    }
//
//    @IBAction func onPlayButtonTouched(_ sender: UIButton) {
//		// Start playing the song
//        playSong()
//    }
//    
//    @IBAction func onPauseButtonTouched(_ sender: UIButton) {
//		// Pause the current song
//        pauseSong()
//    }
//    
//    @IBAction func onStopButtonPressed(_ sender: Any) {
//        // Stop the current song
//        pauseSong()
//        musicPlayer?.currentTime = 0
//        trackAudio()
//    }
//    
//    @IBAction func onSliderTouchStarted(_ sender: UISlider) {
//		// When the UISlider is touched, we stop if from being updated
//        updater.isPaused = true
//    }
//    
//    @IBAction func onSliderValueUpdated(_ sender: UISlider) {
//		// Update the song timer text as the user makes changes
//        songTimerLabel.text = trackTimeString(songSlider.value)
//    }
//    
//    @IBAction func onSliderTouchFinished(_ sender: UISlider) {
//		// Set current time to slider value, update timer text and unpause the update function
//        musicPlayer?.currentTime = Double(songSlider.value)
//        songTimerLabel.text = trackTimeString(songSlider.value)
//        updater.isPaused = false
//    }
//    
//    //MARK: Private Functions
//    private func trackTimeString(_ time : Int) ->String {
//		// Convert argument to minutes:seconds format
//        let minutes = Int(Float(time)/60.0)
//        let seconds = time - minutes*60
//
//        return (NSString(format : "%02d:%02d", minutes, seconds) as String)
//    }
//
//    private func trackTimeString(_ time : Float) ->String {
//		// Convert argument to minutes:seconds format
//        let minutes = Int(time/60)
//        let seconds = Int(time) - minutes*60
//        
//        return (NSString(format : "%02d:%02d", minutes, seconds) as String)
//    }
//    
//    private func playSong() {
//		// Call play on the AVMusicPlayer
//        musicPlayer?.play()
//        
//		// Unpause the update function
//        updater.isPaused = false
//        
//		// Hide play and unhide pause button
//        playButton.isHidden = true
//        playButton.isEnabled = false
//        
//        pauseButton.isHidden = false
//        pauseButton.isEnabled = true
//    }
//    
//    private func pauseSong() {
//		// Call pause on the AVMusicPlayer
//        musicPlayer?.pause()
//        
//		// Pause the update function
//        updater.isPaused = true
//        
//		// Hide pause and unhide play button
//        playButton.isHidden = false
//        playButton.isEnabled = true
//        
//        pauseButton.isHidden = true
//        pauseButton.isEnabled = false
//    }
//    
//    private func loadViews(_ song : Song) {
//		// Set the argument song to the stored variable
//        self.song = song
//        nameTextField.text = song.name
//        authorTextField.text = song.author
//        photoImageView.image = song.photo
//        songSlider.maximumValue = Float(song.duration)
//        songDurationLabel.text = trackTimeString(song.duration)
//        songTimerLabel.text = trackTimeString(Int(songSlider.value))
//    }
//    
//    private func loadSong(_ song : Song) {
//        if let musicItem = NSDataAsset(name: song.urlName) {
//            do {
//                musicPlayer = try AVAudioPlayer(data: musicItem.data, fileTypeHint: AVFileTypeMPEGLayer3)
//                guard let musicPlayer = musicPlayer else {
//                    fatalError("OMG something is not right")
//                }
//                musicPlayer.prepareToPlay()
//                musicPlayer.delegate = self
//            } catch let error {
//                fatalError(error.localizedDescription)
//            }
//        }
//    }
//    
//    private func nextSongItem(_ increment : Int) {
//        songItem += increment
//        
//        let currentPlaylist = shuffle ? shufflePlaylist : playlist
//        
//        if songItem < 0 {
//            songItem = (currentPlaylist.count - 1)
//        }
//        else if songItem >= currentPlaylist.count {
//            songItem = 0
//        }
//        
//        loadViews(currentPlaylist[songItem])a
//        loadSong(self.song!)
//        playSong()
//    }
//    
//    private func resizeFonts(newScale : CGFloat) {
//        let newFontSize = nameTextField.font.pointSize * newScale
//        nameTextField.font = nameTextField.font.withSize(newFontSize)
//        authorTextField.font = authorTextField.font.withSize(newFontSize)
//        songTimerLabel.font = songTimerLabel.font.withSize(newFontSize)
//        songDurationLabel.font = songDurationLabel.font.withSize(newFontSize)
//    }
//	
//	
//	private func createShuffleList() {
//		var newShufflePlaylist = playlist
//        if let index = newShufflePlaylist.index(where: { (item) -> Bool in
//            item.name == song?.name }) {
//			newShufflePlaylist.remove(at: index)
//		
//			shufflePlaylist = newShufflePlaylist.shuffled()
//			shufflePlaylist.insert(song!, at: 0)
//			
//			songItem = 0
//		}
//		else {
//			print("Huoston, we have a problem!")
//		}
//	}
//}
//
