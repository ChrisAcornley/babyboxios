//
//  LaunchScreen.swift
//  BabyBoxiOS
//
//  Created by Christopher Acornley on 31/05/2017.
//  Copyright © 2017 Christopher Acornley. All rights reserved.
//

import UIKit
import AVFoundation

class LaunchScreen: UIViewController, AVAudioPlayerDelegate {

    private var musicPlayer : AVAudioPlayer? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true
        
        // Do any additional setup after loading the view.
        if let musicItem = NSDataAsset(name: "LaunchScreenTrack") {
            do {
                musicPlayer = try AVAudioPlayer(data: musicItem.data, fileTypeHint: AVFileTypeMPEGLayer3)
                guard let musicPlayer = musicPlayer else {
                    fatalError("OMG something is not right")
                }
                musicPlayer.delegate = self
                musicPlayer.prepareToPlay()
                musicPlayer.play()
            } catch let error {
                fatalError(error.localizedDescription)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {

        performSegue(withIdentifier: "ShowMainScreen", sender: nil)
    }
}
