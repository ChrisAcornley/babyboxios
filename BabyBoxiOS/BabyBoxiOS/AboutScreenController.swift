//
//  AboutScreenController.swift
//  BabyBoxiOS
//
//  Created by Christopher Acornley on 27/06/2017.
//  Copyright © 2017 Christopher Acornley. All rights reserved.
//

import UIKit

class AboutScreenController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var aboutTextLabel: UITextView!
    @IBOutlet weak var copyrightLabel: UITextView!
    @IBOutlet weak var backButton: UIButton!
    
    public var scaleUI : CGFloat = 1.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var newFontPoint : Int = Int((copyrightLabel.font?.pointSize)! * scaleUI)
        copyrightLabel.font = UIFont(name: (copyrightLabel.font?.fontName)!, size: CGFloat(newFontPoint))
        
        let aboutString = "Hello and congratulations on the birth of your baby from all of us here at the Royal Scottish National Orchestra!\n\n Introducing your baby to music is easy with the Astar app! Astar is the Gaelic word for journey. We chose it because we hope that music will be a big part of your child’s journey. Sharing rhythms, emotions and playfulness can bring you closer together and help your wee one to develop their language and listening skills. We hope you have lots of fun together, singing and clapping along.\n\n Come and hear us live!\n\n The Royal Scottish National Orchestra play live concerts all year round for people of all ages – both wee kids and big kids! We want everyone to have the chance to enjoy music. Visit our website to find a concert near you.\n rsno.org.uk\n\n The Astar app was developed in partnership with Abertay University"
        
        let attributedText: NSMutableAttributedString = NSMutableAttributedString(string: aboutString)
        
        let linkAttributes: [String : AnyObject] = [
            NSForegroundColorAttributeName : UIColor.blue,
            NSLinkAttributeName: "http://www.abertay.ac.uk" as AnyObject]
        attributedText.addAttributes(linkAttributes, range: NSRange(location: attributedText.length - 18, length: 18))
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        let attributes: [String : Any] = [NSParagraphStyleAttributeName: paragraph]
        attributedText.addAttributes(attributes, range: NSRange(location: 0, length: attributedText.length))
        
        newFontPoint = Int((aboutTextLabel.font?.pointSize)! * scaleUI)
        attributedText.addAttributes([NSFontAttributeName: UIFont.boldSystemFont(ofSize: CGFloat(newFontPoint))], range: NSRange(location: 507, length: 22))
        attributedText.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: CGFloat(newFontPoint))], range: NSRange(location: 0, length: 507))
        attributedText.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: CGFloat(newFontPoint))], range: NSRange(location: 530, length: attributedText.length-530))
        
        aboutTextLabel.delegate = self
        aboutTextLabel.attributedText = attributedText
        
        backButton.contentVerticalAlignment = .fill
        backButton.contentHorizontalAlignment = .fill
    }

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        UIApplication.shared.openURL(URL)
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // On Back Button pressed
    @IBAction func onBackButtonPressed(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
}
