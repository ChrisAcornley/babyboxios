//
//  PlaylistTableViewCell.swift
//  BabyBoxiOS
//
//  Created by Christopher Acornley on 29/05/2017.
//  Copyright © 2017 Christopher Acornley. All rights reserved.
//

import UIKit

class PlaylistTableViewCell: UITableViewCell {

    public var cellType : String = ""
    @IBOutlet weak var playlistBackroundImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
