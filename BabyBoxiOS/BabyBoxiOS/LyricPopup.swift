//
//  LyricPopup.swift
//  BabyBoxiOS
//
//  Created by Christopher Acornley on 28/06/2017.
//  Copyright © 2017 Christopher Acornley. All rights reserved.
//

import UIKit

class LyricPopup: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var authorNameTextBox: UILabel!
    @IBOutlet weak var songNameTextBox: UILabel!
    @IBOutlet weak var lyricsTextBox: UITextView!
    public var song : Song? = nil
    public var scaleUI : CGFloat = 1.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        authorNameTextBox.text = song?.author
        songNameTextBox.text = song?.name
        lyricsTextBox.text = song?.lyricsString
        
        var fontSize : Int = Int(songNameTextBox.font.pointSize * scaleUI)
        songNameTextBox.font = UIFont(name: songNameTextBox.font.fontName, size: CGFloat(fontSize))
        
        fontSize = Int(authorNameTextBox.font.pointSize * scaleUI)
        authorNameTextBox.font = UIFont(name: authorNameTextBox.font.fontName, size: CGFloat(fontSize))
        
        fontSize = Int((lyricsTextBox.font?.pointSize)! * scaleUI)
        lyricsTextBox.font = UIFont(name: (lyricsTextBox.font?.fontName)!, size: CGFloat(fontSize))
        
        backButton.contentVerticalAlignment = .fill
        backButton.contentHorizontalAlignment = .fill
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onBackButtonPressed(_ sender: Any) {
        //self.navigationController!.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    public func resetLabels(newSong: Song, trackDuration: String) {
        song = newSong
        authorNameTextBox.text = song?.author
        songNameTextBox.text = song?.name
        lyricsTextBox.text = song?.lyricsString
    }
}
