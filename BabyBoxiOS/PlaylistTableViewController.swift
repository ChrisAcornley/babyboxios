//
//  PlaylistTableViewController.swift
//  BabyBoxiOS
//
//  Created by Christopher Acornley on 27/06/2017.
//  Copyright © 2017 Christopher Acornley. All rights reserved.
//

import UIKit

class PlaylistTableViewController: UITableViewController {

    @IBOutlet private weak var aboutButton: UIButton!
    
    // Private Var - Declaration
    private var wakePlaylist = [Song]()
    private var playPlaylist = [Song]()
    private var napPlaylist = [Song]()
    private var buttonNames = ["Wake", "Play", "Nap", "All"]
    private var scaleUI : CGFloat = 1.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Make sure navigation bar is hidden, we will do this manually
        self.navigationController?.isNavigationBarHidden = true
        
        // Set the background colour of the table view to be an image instead
        self.tableView.backgroundColor = UIColor(patternImage: UIImage(named: "SongButtonBackground")!)
        
        // Load playlists from JSON files
        loadSongsFromJson(playlistName: "WakePlaylist")
        loadSongsFromJson(playlistName: "PlayPlaylist")
        loadSongsFromJson(playlistName: "NapPlaylist")
        
        scaleUI = self.view.bounds.width / 375.0
        
        aboutButton.contentVerticalAlignment = .fill
        aboutButton.contentHorizontalAlignment = .fill
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // Only one section with four items
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Four Rows for the table
        return 4
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // Cell ID
        let cellIdentifier = "PlaylistTableViewCell"
        
        // Create cell as PlaylistTableViewCell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PlaylistTableViewCell else {
            fatalError("The dequeued cell is not an instance of PlaylistTableViewCell.")
        }
        
        // Set cell type and image
        cell.cellType = buttonNames[indexPath.row]
        cell.playlistBackroundImage.image = UIImage(named: (buttonNames[indexPath.row] + "Image"))
        
        let cellWidth = cell.playlistBackroundImage.bounds.width
        tableView.rowHeight = (cellWidth/25.0)*9.0

        // Return cell for use in table view
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // State we cannot edit the cell views
        return false
    }

    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // State we don't want to allow re-ordering of the table view
        return false
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        
        case "ChoosePlaylist":
            guard let songController = segue.destination as? SongController else {
                fatalError("Unexpected detination: \(segue.destination)")
            }
            
            guard let selectedSongCell = sender as? PlaylistTableViewCell else {
                fatalError("Unexpected sender: \(sender)")
            }
            
            switch(selectedSongCell.cellType) {
            case "Nap":
                songController.playlist = self.napPlaylist
            case "Play":
                songController.playlist = self.playPlaylist
            case "Wake":
                songController.playlist = self.wakePlaylist
            case "All":
                songController.playlist = (self.napPlaylist + self.playPlaylist + self.wakePlaylist)
            default:
                fatalError("Unexpected Button Name")
            }
            
            songController.backgroundType = selectedSongCell.cellType
            songController.scaleUI = self.scaleUI
        case "ShowAbout":
            guard let aboutController = segue.destination as? AboutScreenController else {
                fatalError("Unexpected detination: \(segue.destination)")
            }
            
            aboutController.scaleUI = self.scaleUI
        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
        }
    }
    
    //MARK: Private Methods
    // Load playlist from JSON file
    private func loadSongsFromJson(playlistName : String) {
        if let jsonFile = NSDataAsset(name: playlistName) {
            do {
                let jsonData = try JSONSerialization.jsonObject(with: jsonFile.data, options: [])
                if let jsonResult = jsonData as? [String:Any] {
                    if let array = jsonResult["playlist"] as? [Any] {
                        for song in array {
                            loadSongToPlaylist(songJson: (song as? [String:Any])!, playlistName: playlistName)
                        }
                    }
                }
                
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    // Add a single song to a playlist
    private func loadSongToPlaylist(songJson : [String:Any], playlistName : String) {
        let songName = songJson["name"] as? String
        let songAuthor = songJson["author"] as? String
        let songURL = songJson["url"] as? String
        let songLyrics = songJson["lyrics"] as? String
        
        guard let song = Song(name: songName!, author: songAuthor!, url: songURL!, lyrics: songLyrics!) else {
            fatalError("Cannot create Song object from JSON data")
        }
        
        switch(playlistName) {
        case "WakePlaylist":
            wakePlaylist += [song]
        case "PlayPlaylist":
            playPlaylist += [song]
        case "NapPlaylist":
            napPlaylist += [song]
        default:
            fatalError("Cannot append Song to unknown playlist")
        }
    }
}
